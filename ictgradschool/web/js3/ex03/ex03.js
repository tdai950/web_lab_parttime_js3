"use strict";

// TODO Create object prototypes here

function Album(Title, Artist, Year, Genre, Tracks) {
    this.Title = Title;
    this.Artist = Artist;
    this.Year = Year;
    this.Genre = Genre;
    this.Tracks = Tracks;

}
var swift = new Album('1989', 'Taylor Swift', 2014, 'Pop', [
    'Welcome to New York',
    'Blank Space',
    'Style',
    'Out of the Woods',
    'All You Had to Do Was Stay',
    'Shake it off',
    'I wish you would',
    'Bad blood',
    'Wildest Dreams',
    'How you get the girl',
    'This love',
    'I know places',
    'Clean'
]);

var ptx = new Album('PTX Vol. IV - Classics', 'Pentatonix', 2017, 'Pop', [
    'Bohemian Rhapsody',
    'Imagine',
    'Boogie Woodie Bugle Boy',
    'Over the Rainbow',
    'take On Me',
    'Cant help falling in love',
    'Jolene'
]);

// TODO Create functions here


// TODO Complete the program here

console.log(ptx.Title)

// function getAlbums() {
//     console.log(ptx.Title) 
// };

// function printAlbums(Album)