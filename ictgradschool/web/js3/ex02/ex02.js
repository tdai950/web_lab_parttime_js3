"use strict";

// TODO Car
var car = {
    year: 2007,
    make: 'BMW',
    model: '323i',
    bodyType: 'Sedan',
    Transmission: 'Tiptronic',
    Odometer: 68512,
    Price: '$16000'
};


// TODO Music

var swift = {
    Title: '1989',
    Artist: 'Taylor Swift',
    Year: 2014,
    Genre: 'Pop',
    Tracks: [
        'Welcome to New York',
        'Blank Space',
        'Style',
        'Out of the Woods',
        'All You Had to Do Was Stay',
        'Shake it off',
        'I wish you would',
        'Bad blood',
        'Wildest Dreams',
        'How you get the girl',
        'This love',
        'I know places',
        'Clean'
    ]
};

console.log(swift.Tracks[1]);
console.log(car.make);
console.log("Album: " + swift.Title + ", released in " + swift.Year + " by " + swift.Artist);